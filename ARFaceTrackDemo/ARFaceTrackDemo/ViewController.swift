//
//  ViewController.swift
//  ARFaceTrackDemo
//
//  Created by Prateek Sharma on 6/27/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {

    private var sceneView: ARSCNView { return view as! ARSCNView }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard ARFaceTrackingConfiguration.isSupported
        else { fatalError("Device Should have a True Depth Front Camera") }
        
        DispatchQueue.main.async { [weak self] in
            self?.sceneView.session.run(ARFaceTrackingConfiguration(), options: [])
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
}

extension ViewController: ARSCNViewDelegate {
    /*
      Note: ARSCNFaceGeometry is only available in SceneKit views rendered using Metal,
            which is why you needed to pass in the Metal device during its initialization.
            Also, this code will only compile if you’re targetting real hardware;
            it will not compile if you target a simulator.
    */
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let geometry = ARSCNFaceGeometry(device: MTLCreateSystemDefaultDevice()!)!
        let node = SCNNode(geometry: geometry)
//        node.geometry?.firstMaterial?.diffuse.contents = UIColor.red.withAlphaComponent(0.5)
        node.geometry?.firstMaterial?.fillMode = .lines
        return node
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor,
              let faceGeometry = node.geometry as? ARSCNFaceGeometry
        else { return }
        faceGeometry.update(from: faceAnchor.geometry)
    }
}

